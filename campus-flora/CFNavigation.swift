//
//  CFNavigation.swift
//  campus-flora
//
//  Created by ERU on 8/02/2016.
//  Copyright © 2016 ERU. All rights reserved.
//

import Foundation

class CFNavigation: UINavigationController, UINavigationControllerDelegate {
    var menu: Menu!
    var about: About!
    var speciesSet: JSON!
    var familySet: NSMutableSet! // contains id's of family to show selected
    var curScreen: screenItem!
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        // aesthetics
        //self.navigationBar.tintColor = UIColor.lightTextColor()
        self.navigationBar.barTintColor =  UIColor.rgb(35, 49, 64)
        self.interactivePopGestureRecognizer!.enabled = false
        
        // menu
        menu = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Menu") as! Menu
        
        about = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("About") as! About
        
        // set current screen as home initially
        curScreen = .home
        
        // initialise species and family
        //speciesSet = JSONParser.sharedInstance.species
        familySet = []
        
        // set all families as selected
        for id in JSONParser.sharedInstance.indexF {
            //print("adding (\(family["id"].string!): \(family["name"].string!))")
            familySet.addObject(id.id)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.delegate = self
        //self.view
    }
    
    func navigationController(navigationController: UINavigationController, willShowViewController viewController: UIViewController, animated: Bool) {
        if viewController.isKindOfClass(Map)  && curScreen == .home {
            // make species annotations for map
            let home = viewController as! Map
            speciesSet = nil
            
            home.generateSpeciesAnnotations([]) // slight hack but functional
            
            // add menu
            home.addChildViewController(menu)
            let frame = viewController.view.frame
            menu.view.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, frame.size.height)
            menu.didMoveToParentViewController(viewController)
            
            // add about
            home.addChildViewController(about)
            about.view.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, frame.size.height)
            about.didMoveToParentViewController(viewController)
            
            home.navigationItem.leftBarButtonItem  = UIBarButtonItem(image: UIImage(named: "nav_menu_off"), style: .Plain, target: menu, action: "toggleMenu")
            home.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "about"), style: .Plain, target: about, action: "toggleAbout")
        }
        else if viewController.isKindOfClass(Map) && curScreen == .map {
            // make species annotations for map
            let map = viewController as! Map
    
            map.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "nav_back"), style: .Plain, target: viewController, action: "goBack")
        }
        else if viewController.isKindOfClass(ListFamilies) {
            let family = viewController as! ListFamilies
            //print(familySet)
            family.selectedFamilies = familySet // not sure how necessary this is......
            family.selectionChanged()
            
            // add menu
            family.addChildViewController(menu)
            let frame = viewController.view.frame
            menu.view.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, frame.size.height)
            menu.didMoveToParentViewController(viewController)
            
            // add about
            family.addChildViewController(about)
            about.view.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, frame.size.height)
            about.didMoveToParentViewController(viewController)
            
            family.navigationItem.leftBarButtonItem  = UIBarButtonItem(image: UIImage(named: "nav_menu_off"), style: .Plain, target: menu, action: "toggleMenu")
            family.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "show_map"), style: .Plain, target: family, action: "mapFamilies")
        }
        else if viewController.isKindOfClass(ListSpecies) {
            let species = viewController as! ListSpecies
            
            // add menu
            species.addChildViewController(menu)
            let frame = viewController.view.frame
            menu.view.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, frame.size.height)
            menu.didMoveToParentViewController(viewController)
            
            // add about
            species.addChildViewController(about)
            about.view.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, frame.size.height)
            about.didMoveToParentViewController(viewController)
            
            species.navigationItem.leftBarButtonItem  = UIBarButtonItem(image: UIImage(named: "nav_menu_off"), style: .Plain, target: menu, action: "toggleMenu")
            //species.navigationItem.titleView = species.search.searchBar
            //species.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "showMap"), style: .Plain, target: species, action: "mapSpecies")
        }
        else if viewController.isKindOfClass(ListTrails) {
            let trails = viewController as! ListTrails
            
            // add menu
            trails.addChildViewController(menu)
            let frame = viewController.view.frame
            menu.view.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, frame.size.height)
            menu.didMoveToParentViewController(viewController)
            
            // add about
            trails.addChildViewController(about)
            about.view.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, frame.size.height)
            about.didMoveToParentViewController(viewController)
            
            trails.navigationItem.leftBarButtonItem  = UIBarButtonItem(image: UIImage(named: "nav_menu_off"), style: .Plain, target: menu, action: "toggleMenu")
        }
        else if viewController.isKindOfClass(ListFavourites) {
            let fav = viewController as! ListFavourites
            
            // add menu
            fav.addChildViewController(menu)
            let frame = viewController.view.frame
            menu.view.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, frame.size.height)
            menu.didMoveToParentViewController(viewController)
            
            // add about
            fav.addChildViewController(about)
            about.view.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, frame.size.height)
            about.didMoveToParentViewController(viewController)
            
            fav.navigationItem.leftBarButtonItem  = UIBarButtonItem(image: UIImage(named: "nav_menu_off"), style: .Plain, target: menu, action: "toggleMenu")

        }
        else if viewController.isKindOfClass(Detail) {
            let detail = viewController as! Detail
            detail.navigationItem.leftBarButtonItem  = UIBarButtonItem(image: UIImage(named: "nav_back"), style: .Plain, target: viewController, action: "goBack")
            detail.navigationItem.title = ""
            
            // add right icons
            var rightButtons: [UIBarButtonItem] = [detail.favButton]
            for i in  1 ... detailRightIcons.count - 1 {
                //print("index is: \(i)")
                let button = UIBarButtonItem(image: UIImage(named: detailRightIcons[i]), style: .Plain, target: viewController, action: Selector(detailRightActions[i]))
                rightButtons.append(button)
            }
            detail.navigationItem.rightBarButtonItems = rightButtons
        }
        
        else {
            //viewController.navigationItem.leftBarButtonItem  = UIBarButtonItem(image: UIImage(named: "nav_back"), style: .Plain, target: viewController, action: "goHome")
            print("IT REALLY SHOULD NOT REACH HERE!!!!")
        }
    }
    
    // update family selected set here!
    func setSelectedFamilies(families: NSMutableSet) {
        familySet = families
    }
    
    // for main views
    func checkScreen(screen: screenItem) -> Bool {
        var found = false
        
        for vc in self.viewControllers {
            if (screen == .home && vc.isKindOfClass(Map)) {
                self.popToViewController(vc, animated: true)
                found = true
            }
            else if (screen == .species && vc.isKindOfClass(ListSpecies)) {
                self.popToViewController(vc, animated: true)
                found = true
            }
            else if (screen == .families && vc.isKindOfClass(ListFamilies)) {
                self.popToViewController(vc, animated: true)
                found = true
            }
            else if (screen == .trails && vc.isKindOfClass(ListTrails)) {
                self.popToViewController(vc, animated: true)
                found = true
            }
            
            else if (screen == .favourites && vc.isKindOfClass(ListFavourites)) {
                self.popToViewController(vc, animated: true)
                found = true
            }
            
            if (found) {
                break
            }
        }
            
        return found
    }
    
    // for map
    func checkScreen(screen: screenItem, current: screenItem, sp: [String]) -> Bool {
        var found = false
        for vc in self.viewControllers {
            if (screen == .map && vc.isKindOfClass(Map)) { // this could be a point of error
                let map = vc as! Map
                // if there already exists a map type instead of home, edit current one, otherwise make new
                if (map.mode == .map) {
                    map.generateSpeciesAnnotations(sp)
                    map.mode = screenItem.map
                    self.popToViewController(vc, animated: true)
                    found = true
                    break
                }
            }
        }
        
        return found
    }
    
    // for detail
    func checkScreen(screen: screenItem, current: screenItem, sp: JSON) -> Bool {
        var found = false
        for vc in self.viewControllers {
            if (screen == .detail && vc.isKindOfClass(Detail)) {
                let det = vc as! Detail
                det.species = sp
                det.favButton = UIBarButtonItem(image: UIImage(named: detailRightIcons[0]), style: .Plain, target: vc, action: Selector(detailRightActions[0]))
                self.popToViewController(vc, animated: true)
                found = true
            }
        }
        
        return found
    }
    
    // for the main views
    func nextScreen(screen: screenItem) {
        switch screen {
        case .home:
            if !checkScreen(.home) {
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Map") as! Map
                vc.previousScreen = nil
                vc.mode = .home
                self.pushViewController(vc, animated: true)
            }

        case .species:
            if !checkScreen(.species) {
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("ListSpecies") as! ListSpecies
                self.pushViewController(vc, animated: true)
            }
        
        case .families:
            if !checkScreen(.families) {
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("ListFamilies") as! ListFamilies
                self.pushViewController(vc, animated: true)
            }
        case .trails:
            if !checkScreen(.trails) {
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("ListTrails") as! ListTrails
                self.pushViewController(vc, animated: true)
            }
        
        case .favourites:
            if !checkScreen(.favourites) {
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("ListFavourites") as! ListFavourites
                self.pushViewController(vc, animated: true)
            }
        default:
            print("this shouldn't be possible")
        }
        
        // store the next screen as the current screen
        curScreen = screen
    }
    
    // for map
    func nextScreen(screen: screenItem, current: screenItem, sp: [String]) {
        if !checkScreen(.map, current: current, sp: sp) {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Map") as! Map
            vc.generateSpeciesAnnotations(sp)
            vc.previousScreen = current
            vc.mode = screenItem.map
            curScreen = screen
            self.pushViewController(vc, animated: true)
        }
    }
    
    // for detail
    func nextScreen(screen: screenItem, current: screenItem, sp: JSON) {
        if !checkScreen(.detail, current: current, sp: sp) {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Detail") as! Detail
            vc.species = sp
            vc.previousScreen = current
            vc.favButton = UIBarButtonItem(image: UIImage(named: detailRightIcons[0]), style: .Plain, target: vc, action: Selector(detailRightActions[0]))
            curScreen = screen
            self.pushViewController(vc, animated: true)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}