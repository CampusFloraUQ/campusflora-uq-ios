//
//  Map.swift
//  CampusFlora
//
//  Created by ERU on 16/11/2015.
//  Copyright © 2015 Campus Flora. All rights reserved.
//

import UIKit
import MapKit

class Map: UIViewController, CLLocationManagerDelegate {
    
    var manager: CLLocationManager!
    var clusterer: FBClusteringManager = FBClusteringManager()
    var info: InfoViewCF!
    var previousScreen: screenItem!
    var mode: screenItem = .home
    var species: [String]!
    
    // only set the map once
    var mapOnce = false
    
    // search stuff
    var annotations: [FBAnnotation]!
    var filteredAnnotations: [FBAnnotation]!
    //var species: [Species]!
    //var filteredSpecies: [Species]!
    
    var search: UISearchController!
    
    @IBOutlet weak var MapView: MKMapView!
    @IBOutlet weak var MapType: UISegmentedControl!
    
    @IBOutlet weak var navBar: UINavigationItem!
    
    override func viewDidLayoutSubviews() {
        if (mode == .map && mapOnce) {
            self.MapView.showAnnotations(clusterer.allAnnotations(), animated: false)
            mapOnce = false
        }
        
        let annotat = self.MapView.viewForAnnotation(self.MapView.userLocation)
        annotat?.superview?.bringSubviewToFront(annotat!)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)

        self.MapView.setRegion(self.MapView.region, animated: false)
        mapOnce = true
    }
    
    override func viewWillDisappear(animated: Bool) {
        //self.navigationItem.titleView = nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.title = species["genusSpecies"].string!
        
        self.MapView.delegate = self
        
        MapView.mapType = MKMapType.Satellite
        MapType.selectedSegmentIndex = 0
        
        // add the clusterer
        //clusterer = FBClusteringManager()
        
        manager = CLLocationManager()
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.requestWhenInUseAuthorization()
        
        info = NSBundle.mainBundle().loadNibNamed("InfoViewCF", owner: self, options: nil).first! as! InfoViewCF
        
        MapType.addTarget(self, action: "mapTypeChanged:", forControlEvents: .ValueChanged)
        
        let tap = UITapGestureRecognizer(target: self, action: "goToSpecies")
        info.addGestureRecognizer(tap)
        
        let region = MKCoordinateRegionMake(CLLocationCoordinate2D(latitude: -33.858847985624791, longitude: 150.97626055780592), MKCoordinateSpan(latitudeDelta: 1.0694568385755048, longitudeDelta: 0.79342125862950752))
        self.MapView.setRegion(self.MapView.regionThatFits(region), animated: false)
        
        if (mode == .home) {
            search = UISearchController(searchResultsController: nil)
            search.searchResultsUpdater = self
            search.searchBar.delegate = self
            search.dimsBackgroundDuringPresentation = false
            definesPresentationContext = true
            search.searchBar.sizeToFit()
            self.navigationItem.titleView = search.searchBar
            search.hidesNavigationBarDuringPresentation = false
            search.loadViewIfNeeded()
        }
    }
    
    // search
    func filterContentForSearchText(searchText: String, scope: String = "All") {
        if (searchText == "") {
            clusterer.setAnnotations(annotations)
        }
        else {
            // filter species
            filteredAnnotations = annotations.filter { annot in
                return (annot.subtitle?.lowercaseString.containsString(searchText.lowercaseString))!
                    || (annot.title?.lowercaseString.containsString(searchText.lowercaseString))!
                    || (annot.info?.lowercaseString.containsString(searchText.lowercaseString))!
            }
            
            clusterer.setAnnotations(filteredAnnotations)
        }
        
        // to refresh the map
        MapView.setCenterCoordinate(MapView.centerCoordinate, animated: false)
    }
    
    // Segment
    func mapTypeChanged(segment: UISegmentedControl) {
        if (MapType.selectedSegmentIndex == 0) {
            MapView.mapType = MKMapType.Satellite
        }
        else {
            MapView.mapType = MKMapType.Standard
        }
    }
    

    // create annotations
    func generateSpeciesAnnotations(sp: [String]) { //this needs to be changed to cater for family and trails
        //print("generating map again and again")
        
        //if (speciesSet)
        
        annotations = []
        species = sp
        if (sp.count > 0) {
            print("selected species are: ")
            print(species)
            annotations = JSONParser.sharedInstance.annotations.filter { annot in
                return species.contains(annot.id!)
            }
        }
        else {
            annotations = JSONParser.sharedInstance.annotations
        }
        
        // make sure info doesn't appear before the view is viewed
        if (info != nil) {
            info.removeFromSuperview()
        }
        
        clusterer.setAnnotations(annotations)
    }
    
    // Display annotations/clusters
    func mapView(mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        NSOperationQueue().addOperationWithBlock({
            let mapBoundsWidth = Double(self.MapView!.bounds.size.width)
            let mapRectWidth:Double = self.MapView!.visibleMapRect.size.width
            let scale:Double = mapBoundsWidth / mapRectWidth

            let annotationArray = self.clusterer.clusteredAnnotationsWithinMapRect(self.MapView.visibleMapRect, withZoomScale:scale)
            self.clusterer.displayAnnotations(annotationArray, onMapView:self.MapView)
        })
        
        // set max zoom capacity
        //print("current region is: \(mapView.region)")
        if (mapView.region.span.latitudeDelta <= max_span.latitudeDelta
            || mapView.region.span.longitudeDelta <= max_span.longitudeDelta) {
            //print("MAX ZOOM REACHED!")
            mapView.setRegion(MKCoordinateRegion(center: mapView.region.center, span: max_span), animated: animated)
        }
    }
    
    
    // cllocation manager
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        //print("authorization status changed to: \(status.hashValue)")
        if (status == .AuthorizedWhenInUse || status == .AuthorizedAlways) {
            // display the user
            MapView.showsUserLocation = true
            manager.startUpdatingLocation()
        }
    }

    // go to species
    func goToSpecies() {
        let speciesInfo = JSONParser.sharedInstance.findSpeciesByID(info.id!)
        let nav = navigationController as! CFNavigation
        if mode == .map {
            nav.nextScreen(.detail, current: previousScreen, sp: speciesInfo)
        }
        else {
            nav.nextScreen(.detail, current: .home, sp: speciesInfo)
        }
    }
    
    // go back
    func goBack() {
        //print("What is previous screen at this point????? \(previousScreen)")
        if (previousScreen != nil && mode == .map) {
            let nav = navigationController as! CFNavigation
            nav.nextScreen(previousScreen)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension Map: MKMapViewDelegate {
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        // try to dequeue an existing pin view first
        // if an existing pin view was not available, create one
        //no customized annotation view if it is current location
        
        if (annotation.isKindOfClass(MKUserLocation)) {
            let annotat = self.MapView.viewForAnnotation(annotation)
            annotat?.superview?.bringSubviewToFront(annotat!)
            return nil
        }
        
        if annotation.isKindOfClass(FBAnnotationCluster) {
            //print("Showing cluster view!")
            var clusterView = MapView.dequeueReusableAnnotationViewWithIdentifier(clusterIdentifier)
            clusterView = FBAnnotationClusterView(annotation: annotation, reuseIdentifier: clusterIdentifier)
            return clusterView
        }
        else {
            var pinView = MapView.dequeueReusableAnnotationViewWithIdentifier(pinIdentifier) as? CFAnnotationView
            if (pinView == nil) {
                //print("Showing pin view!")
                //let annot = annotation as? FBAnnotation
                pinView = CFAnnotationView(annotation: annotation, reuseIdentifier: pinIdentifier)
                pinView!.pinTintColor = UIColor.greenColor()
                /*
                let iv = UIImageView(image: UIImage(named: "pin.jpg"))
                iv.layer.cornerRadius = (iv.image?.size.height)!/2
                iv.layer.masksToBounds = true
                if (annot?.imageURL != nil) {
                    JMImageCache.sharedCache().imageForURL(NSURL(string: (annot?.imageURL)!),
                        completionBlock: { (image) in
                        iv.image = image.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
                    })
                }
                
                pinView?.leftCalloutAccessoryView = iv
                pinView?.canShowCallout = false
                pinView?.enabled = true
                let rightButton = UIButton(type: .InfoDark)
                pinView?.rightCalloutAccessoryView = rightButton*/
            }
            
            return pinView
        }
    }

    func mapView(mapView: MKMapView, didSelectAnnotationView view: MKAnnotationView) {
        if (view.annotation!.isKindOfClass(FBAnnotationCluster) || (view.annotation?.isKindOfClass(MKUserLocation))!) {
            return
        }
        
        let ann = view.annotation as! FBAnnotation
        
        info.Title.text = ann.title
        info.Subtitle.text = ann.subtitle
        info.Info.text = ann.info
        info.id = ann.id

        if (ann.imageURL != nil) {
            JMImageCache.sharedCache().imageForURL(NSURL(string: (ann.imageURL)!),
                completionBlock: { (image) in
                    self.info.Image.image = image.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
            })
        }
        else {
            info.Image.image = UIImage(named: "pin.jpg")
        }
        
        info.sizeToFit()
        
        info.center = CGPointMake(view.bounds.size.width*0.5 - 8, view.bounds.size.height*0.5 - 50)
        view.addSubview(info)
    }
    
    func mapView(mapView: MKMapView, didDeselectAnnotationView view: MKAnnotationView) {
        info.removeFromSuperview()
    }
}

extension Map: UISearchResultsUpdating {
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        filterContentForSearchText(searchController.searchBar.text!)
    }
}

extension Map: UISearchBarDelegate {
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        clusterer.setAnnotations(annotations)
        // to refresh the map
        MapView.setCenterCoordinate(MapView.centerCoordinate, animated: false)
    }
}