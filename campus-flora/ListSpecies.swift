//
//  List.swift
//  CampusFloraIOS
//
//  Created by ERU on 12/11/2015.
//  Copyright © 2015 ERU. All rights reserved.
//

import UIKit

class ListSpecies: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var index: [Index]!
    var filteredIndex: [Index]!
    var families: [String: [Species]]!
    var filteredFamilies: [String: [Species]]!
    var species: [Species]!
    var filteredSpecies: [Species]!
    
    var search: UISearchController!
    
    @IBOutlet weak var Table: UITableView!
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        search.searchBar.becomeFirstResponder()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Table.registerClass(UITableViewCell.self, forCellReuseIdentifier: speciesIdentifier)
        Table.delegate = self
        Table.dataSource = self
        
        //families = JSONParser.sharedInstance.families
        index = JSONParser.sharedInstance.indexF
        species = JSONParser.sharedInstance.speciesArray
        families = JSONParser.sharedInstance.familySpecies
        
        search = UISearchController(searchResultsController: nil)
        search.searchResultsUpdater = self
        search.dimsBackgroundDuringPresentation = false
        search.searchBar.sizeToFit()
        //self.navigationItem.titleView = search.searchBar
        Table.tableHeaderView = search.searchBar
        search.hidesNavigationBarDuringPresentation = false
        search.loadViewIfNeeded()
        definesPresentationContext = true
    }
    
    
    func filterContentForSearchText(searchText: String, scope: String = "All") {
        // filter species
        filteredSpecies = species.filter { species in
            return species.genusSpecies.lowercaseString.containsString(searchText.lowercaseString) || species.commonName.lowercaseString.containsString(searchText.lowercaseString)
        }
        
        // filter families
        filteredFamilies = [:]
        for sp in filteredSpecies {
            if (filteredFamilies[sp.family] == nil) {
                filteredFamilies[sp.family] = []
            }
            filteredFamilies[sp.family]?.append(sp)
        }
        
        // filter index
        filteredIndex = index.filter({ (index) -> Bool in
            filteredFamilies.keys.contains(index.name)
        })
        
        Table.reloadData()
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if search.active && search.searchBar.text != "" {
            return filteredFamilies.count
        }

        return families.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if search.active && search.searchBar.text != "" {
            return (filteredFamilies[filteredIndex[section].name]?.count)!
        }

        
        return families[index[section].name]!.count
        //return families[index]
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if search.active && search.searchBar.text != "" {
            return filteredIndex[section].name
        }
        
        
        return index[section].name
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: speciesIdentifier)
        
        if search.active && search.searchBar.text != "" {
            //print("blarhjsghjldfs: \(filteredIDs[indexPath.row])")
            cell.textLabel!.text = filteredFamilies[filteredIndex[indexPath.section].name]![indexPath.row].genusSpecies
            cell.detailTextLabel!.text = filteredFamilies[filteredIndex[indexPath.section].name]![indexPath.row].commonName
            cell.imageView!.image = UIImage(named: filteredFamilies[filteredIndex[indexPath.section].name]![indexPath.row].imageName)
            if (filteredFamilies[filteredIndex[indexPath.section].name]![indexPath.row].imageName != "noImageAvailable-thumb") {
                JMImageCache.sharedCache().imageForURL(NSURL(string: filteredFamilies[filteredIndex[indexPath.section].name]![indexPath.row].imageName)!, completionBlock: { (image) in
                    cell.imageView!.image = image
                })
                cell.imageView!.contentMode = UIViewContentMode.ScaleAspectFit
                //cell.imageView!.layer.cornerRadius = (cell.imageView!.image?.size.height)!/2
                //cell.imageView!.layer.masksToBounds = true
            }
            cell.accessoryView = UIImageView(image: UIImage(named: "species_detail"))
        }
        else {
            //print("blarhjsghjldfs: \(species[indexPath.row])")
            cell.textLabel!.text = families[index[indexPath.section].name]![indexPath.row].genusSpecies
            cell.detailTextLabel!.text = families[index[indexPath.section].name]![indexPath.row].commonName
            cell.imageView!.image = UIImage(named: families[index[indexPath.section].name]![indexPath.row].imageName)
            if (families[index[indexPath.section].name]![indexPath.row].imageName != "noImageAvailable-thumb") {
                JMImageCache.sharedCache().imageForURL(NSURL(string: families[index[indexPath.section].name]![indexPath.row].imageName)!, completionBlock: { (image) in
                    cell.imageView!.image = image
                })
                cell.imageView!.contentMode = UIViewContentMode.ScaleAspectFit
            }
            cell.accessoryView = UIImageView(image: UIImage(named: "species_detail"))
        }
        
            
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        //let cell = tableView.cellForRowAtIndexPath(indexPath)
        //print("selected:")
        //print(cell)
        
        let info: JSON!
        
        if search.active && search.searchBar.text != "" {
            info = JSONParser.sharedInstance.findSpeciesByID(filteredFamilies[filteredIndex[indexPath.section].name]![indexPath.row].id)
        }
        else {
            info = JSONParser.sharedInstance.findSpeciesByID(families[index[indexPath.section].name]![indexPath.row].id)
        }
        
        let nav = navigationController as! CFNavigation
        nav.nextScreen(.detail, current: .species, sp: info)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension ListSpecies: UISearchResultsUpdating {
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        filterContentForSearchText(searchController.searchBar.text!)
    }
}

