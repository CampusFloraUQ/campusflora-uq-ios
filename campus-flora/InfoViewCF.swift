//
//  InfoViewCF.swift
//  campus-flora
//
//  Created by ERU on 23/02/2016.
//  Copyright © 2016 ERU. All rights reserved.
//

import Foundation
import MapKit

class InfoViewCF: UIView {
    @IBOutlet weak var Title: UILabel!
    @IBOutlet weak var Subtitle: UILabel!
    @IBOutlet weak var Info: UILabel!
    @IBOutlet weak var Image: UIImageView!
    
    var id: String!
    
    override func sizeToFit() {
        self.Title.sizeToFit()
        self.Subtitle.sizeToFit()
        self.Info.sizeToFit()
        super.sizeToFit()
    }
 }

class CFAnnotationView: MKPinAnnotationView {
    override func hitTest(point: CGPoint, withEvent event: UIEvent?) -> UIView? {
        let hitView = super.hitTest(point, withEvent: event)
        
        if (hitView != nil) {
            self.superview?.bringSubviewToFront(self)
        }
        
        return hitView
    }
    
    override func pointInside(point: CGPoint, withEvent event: UIEvent?) -> Bool {
        let rect = self.bounds
        var isInside = CGRectContainsPoint(rect, point)
        
        if (!isInside) {
            for v in self.subviews {
                isInside = CGRectContainsPoint(v.frame, point)
                if (isInside) {
                    break
                }
            }
        }
        
        return isInside
    }
}