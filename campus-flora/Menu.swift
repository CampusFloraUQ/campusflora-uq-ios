//
//  Menu.swift
//  CampusFlora
//
//  Created by ERU on 16/11/2015.
//  Copyright © 2015 Campus Flora. All rights reserved.
//

import UIKit

class Menu: UIViewController, UITableViewDataSource, UITableViewDelegate, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var MenuItems: UITableView!
    @IBOutlet weak var BlankSpace: UIView!
    
    var show = true

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(Menu.toggleMenu))
        tap.cancelsTouchesInView = false
        BlankSpace.addGestureRecognizer(tap)
        tap.delegate = self
        
        let header = UIImageView()
        let myImage: UIImage = UIImage(named: "menuLogo")!
        header.image = myImage
        header.frame = CGRectMake(0, 0, MenuItems.frame.width, 170)
        MenuItems.tableHeaderView = header
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        MenuItems.delegate = self
        MenuItems.dataSource = self
    }
    
    // toggle
    func toggleMenu() {
        //print("IN TOGGLE show value is: \(show)")
        if (show) {
            let parent = self.parentViewController?.view
            parent!.addSubview(view)
            self.parentViewController?.navigationItem.leftBarButtonItem?.image = UIImage(named: "nav_menu_on")
            show = false
        }
        else {
            view.removeFromSuperview()
            self.parentViewController?.navigationItem.leftBarButtonItem?.image = UIImage(named: "nav_menu_off")
            show = true
        }
    }
    
    // table
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuList.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: menuIdentifier)
        cell.imageView?.image = UIImage(named: menuIcons[indexPath.row])
        cell.textLabel?.text = menuList[indexPath.row]
        cell.textLabel?.font = UIFont.systemFontOfSize(30)
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let nav = navigationController as! CFNavigation
        
        //show = true // make sure it starts off as ought to be toggled
        //print("wtfffff \(indexPath.row) && \(nav.curScreen)")
        
        if (indexPath.row == 0 && nav.curScreen != .home) {
            toggleMenu()
            nav.nextScreen(.home)
        }
        else if (indexPath.row == 1 && nav.curScreen != .species) {
            toggleMenu()
            nav.nextScreen(.species)
        }
        else if (indexPath.row == 2 && nav.curScreen != .families) {
            toggleMenu()
            nav.nextScreen(.families)
        }
        else if (indexPath.row == 3 && nav.curScreen != .trails) {
            toggleMenu()
            nav.nextScreen(.trails)
        }
        else if (indexPath.row == 4 && nav.curScreen != .favourites) {
            toggleMenu()
            nav.nextScreen(.favourites)
        }
        else {
            //show = false // false alarm menu can resume regular function since it didn't change views
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}