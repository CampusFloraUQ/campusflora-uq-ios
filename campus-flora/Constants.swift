//
//  Constants.swift
//  CampusFlora
//
//  Created by ERU on 12/11/2015.
//  Copyright © 2015 Campus Flora. All rights reserved.
//

import Foundation
import MapKit

let speciesIdentifier = "speciesItem"
let familyIdentifier = "familyItem"
let trailIdentifier = "trailItem"
let favouriteIdentifier = "favouriteItem"

let clusterIdentifier = "annotationClusterItem"
let pinIdentifier = "annotationPinItem"

let menuIdentifier = "menuItem"

enum screenItem: String {
    case home = "Home"
    case map = "Map"
    case species = "Species"
    case families = "Families"
    case trails = "Trails"
    case favourites = "Favourites"
    case detail = "Detail"
    case about = "About"
}

let menuList = [
    screenItem.home.rawValue,
    screenItem.species.rawValue,
    screenItem.families.rawValue,
    screenItem.trails.rawValue,
    screenItem.favourites.rawValue
]

let menuIcons = [
    "menu_home",
    "menu_species",
    "menu_families",
    "menu_trails",
    "menu_favourites"
]

let max_span = MKCoordinateSpan(latitudeDelta: 0.0015021723335308934, longitudeDelta: 0.0011104346521619846)

let FavouriteSpecies = "UDFavouriteSpecies"

let detailRightActions = [
    "toggleFav",
    "showMap",
    "sendMail",
    "shareSpecies"
]

let detailRightIcons = [
    "favourite",
    "show_map",
    "feedback",
    "share"
]