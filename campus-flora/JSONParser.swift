//
//  JSONParser.swift
//  CampusFlora
//
//  Created by ERU on 12/11/2015.
//  Copyright © 2015 Campus Flora. All rights reserved.
//

import Foundation
import MapKit

public class JSONParser {
    static let sharedInstance = JSONParser()
    
    let speciesPath = NSBundle.mainBundle().pathForResource("species", ofType: "json")
    let familiesPath = NSBundle.mainBundle().pathForResource("families", ofType: "json")
    let trailsPath = NSBundle.mainBundle().pathForResource("trails", ofType: "json")

    let indexS: [Index]!
    let indexF: [Index]!
    let indexT: [Index]!
    
    let species: JSON!
    let families: JSON!
    let trails: JSON!
    
    var speciesArray: [Species]!
    var familySpecies: [String : [Species]]!
    
    let totalSpecies: Int!
    let totalFamilies: Int!
    let totalPlants: Int!

    // holds all annotations
    let annotations: [FBAnnotation]!
    
    // rearrange json data to improve and make access more efficient
    private init() {
        
        let tempS = JSON(data: NSData(contentsOfFile: NSBundle.mainBundle().pathForResource("species", ofType: "json")!)!)
        let tempF = JSON(data: NSData(contentsOfFile: NSBundle.mainBundle().pathForResource("families", ofType: "json")!)!)
        let tempT = JSON(data: NSData(contentsOfFile: NSBundle.mainBundle().pathForResource("trails", ofType: "json")!)!)
      
        //species = tempS
        //families = tempF
        //trails = tempT
        
        /*
        // sorting
        let temp = tempF.arrayValue.sort { $0["id"].intValue < $1["id"].intValue}
        families = JSON(temp)
        */
        
        var tmp: [String: JSON] = [:]
        var indexTmp: [Index] = []
        
        var totalTmpPlants = 0
        var totalTmp = 0
        speciesArray = []
        familySpecies = [:]
        
        // species improve
        
        
        
        for (_, sp): (String, JSON) in tempS {
            //print("\(fam["id"].intValue)")
            tmp[sp["id"].stringValue] = sp
            indexTmp.append(Index(id: sp["id"].stringValue, name: sp["genusSpecies"].stringValue)) // form an index
            /*
            var imageName = "noImageAvailable-thumb"
            if (sp["images"].count > 0) {
                imageName = "http://campusflora.sydneybiology.org" + sp["images"][0]["image_url_thumb_retina"].string! 
            }
            //speciesArray.append(Species(genusSpecies: sp["genusSpecies"].stringValue, commonName: sp["commonName"].stringValue, id: sp["id"].stringValue, imageName: imageName))*/
            totalTmp = totalTmp + 1
            totalTmpPlants = totalTmpPlants + sp["species_locations"].arrayValue.count
        }
        
        totalSpecies = totalTmp
        totalPlants = totalTmpPlants
        
        // make the index alphabetical
        indexS = indexTmp.sort { (a, b) -> Bool in
            return a.name < b.name
        }
        
        species = JSON(tmp)

        // families improve
        tmp = [:]
        indexTmp = []
        totalTmp = 0
        for (_, fam): (String, JSON) in tempF {
            tmp[fam["id"].stringValue] = fam
            /*var tmpSp: [JSON] = []
            for (_, sp): (String, JSON) in fam["species"] {
                let elem = species[sp["id"].stringValue]
                tmpSp.append(JSON(["id": elem["id"], "images": elem["images"], "genusSpecies": elem["genusSpecies"], "commonName": elem["commonName"]]))
            }
            tmp[fam["id"].stringValue]!["speciesDetail"] = JSON(tmpSp) // add species detail here to make list species better for search string filtering*/
            indexTmp.append(Index(id: fam["id"].stringValue, name: fam["name"].stringValue))
            
            for (_, spec): (String, JSON) in fam["species"] {
                let sp = species[spec["id"].stringValue]
                var imageName = "noImageAvailable-thumb"
                if (sp["images"].count > 0) {
                    imageName = "http://campusflora.sydneybiology.org" + sp["images"][0]["image_url_thumb_retina"].string!
                }
                let tmp = Species(genusSpecies: sp["genusSpecies"].stringValue, commonName: sp["commonName"].stringValue, id: sp["id"].stringValue, imageName: imageName, family: fam["name"].stringValue)
                //print(tmp)
                speciesArray.append(tmp)
            }
            totalTmp = totalTmp + 1
        }
        
        totalFamilies = totalTmp
        
        indexF = indexTmp.sort { (a, b) -> Bool in
            return a.name < b.name
        }
        
        for sp in speciesArray {
            if (familySpecies[sp.family] == nil) {
                familySpecies[sp.family] = []
            }
            familySpecies[sp.family]?.append(sp)
        }
        
        
        families = JSON(tmp)
        
        // trails improve
        tmp = [:]
        indexTmp = []
        for (_, tr): (String, JSON) in tempT {
            tmp[tr["id"].stringValue] = tr
            indexTmp.append(Index(id: tr["id"].stringValue, name: tr["name"].stringValue))
        }
        
        // make the index alphabetical
        indexT = indexTmp.sort { (a, b) -> Bool in
            return a.name < b.name
        }
        
        trails = JSON(tmp)
        
        // favourites setup first time only
        let defaults = NSUserDefaults.standardUserDefaults()
        let dict = defaults.dictionaryForKey(FavouriteSpecies)
        if (dict == nil) {
            var tmp: [String: Bool] = [:]
            for (_, sp): (String, JSON) in tempS {
                tmp[sp["id"].stringValue] = false
            }
            defaults.setObject(tmp, forKey: FavouriteSpecies)
        }
        
        //pre-generate annotations
        var annots: [FBAnnotation] = []
        for (_, spec): (String, JSON) in species {
            for (_, location): (String, JSON) in spec["species_locations"] {
                let annotation = FBAnnotation()
                if (location["lat"].string != nil && location["lon"].string != nil) {
                    annotation.coordinate = CLLocationCoordinate2D(latitude: NSString(string: location["lat"].string!).doubleValue, longitude: NSString(string: location["lon"].string!).doubleValue)
                    annotation.id = spec["id"].stringValue
                    annotation.title = spec["genusSpecies"].string!
                    annotation.subtitle = spec["commonName"].string!
                    
                    if (location["arborplan_id"].string != nil && !location["arborplan_id"].string!.isEmpty) {
                        annotation.info = "Arborplan: " + location["arborplan_id"].string!
                    }
                    
                    if (spec["images"].count > 0) {
                        annotation.imageURL = "http://campusflora.sydneybiology.org" + spec["images"][0]["image_url_thumb_retina"].string!
                    }
                    else {
                        annotation.imageURL = nil
                    }
                    //print("image url: \(annotation.imageURL)")
                    annots.append(annotation)
                }
            }
        }
        
        annotations = annots
    }
    
    func findSpeciesByID(id: String) -> JSON {
        if !species[id].isEmpty {
           return species[id]
        }
        
        return nil
    }
    
    func findFamilyByID(id: String) -> JSON {
        if !families[id].isEmpty {
            return families[id]
        }
        
        return nil
    }
    
    func findTrailsByID(id: String) -> JSON {
        if !trails[id].isEmpty {
            return trails[id]
        }
        
        return nil
    }

    
    func findSpeciesFromFamilies(ids: NSSet) -> [String] {
        let speciesSet = NSMutableSet()
        
        for id in ids {
            //print("find species from families \(id)")
            let fam = findFamilyByID(id as! String)
            //print("family is: ")
            //print(fam["name"])
            //print(fam["species"])
            for  (_, spID): (String, JSON) in fam["species"] {
                //print("species id is: \(spID["id"])")
                speciesSet.addObject(spID["id"].stringValue)
            }
        }
        
        return speciesSet.allObjects as! [String]
    }
    
    func findSpeciesFromTrail(id: String) -> [String] {
        let speciesSet = NSMutableSet()
        
        let trail = findTrailsByID(id)
        
        for (_, spID): (String, JSON) in trail["species"] {
            speciesSet.addObject(spID["id"].stringValue)
        }
        
        return speciesSet.allObjects as! [String]
    }
    
    // nsuserdefaults for favourite functionality
    func speciesFavourited(spID: String) -> Bool {
        let defaults = NSUserDefaults.standardUserDefaults()
        let favs = defaults.dictionaryForKey(FavouriteSpecies) as! [String : Bool]

        return favs[spID]!
    }
    
    func toggleFavourites(spID: String) {
        let defaults = NSUserDefaults.standardUserDefaults()
        var favs = defaults.dictionaryForKey(FavouriteSpecies) as! [String : Bool]

        if (favs[spID]! == false) {
            favs[spID] = true
        }
        else {
            favs[spID] = false
        }
        //print("fav array is now:")
        //print(favs)
        defaults.setObject(favs, forKey: FavouriteSpecies)
    }
    
    // might want to edit this so that it 
    func speciesFromFavourites() -> [Species] {
        let defaults = NSUserDefaults.standardUserDefaults()
        let favs = defaults.dictionaryForKey(FavouriteSpecies) as! [String : Bool]
        
        var spArr: [Species] = []
        for id in favs.keys {
            if favs[id] == true {
                let sp = findSpeciesByID(id)
                var imageName = "noImageAvailable-thumb"
                if (sp["images"].count > 0) {
                    imageName = "http://campusflora.sydneybiology.org" + sp["images"][0]["image_url_thumb_retina"].string!
                }
                spArr.append(Species(genusSpecies: sp["genusSpecies"].stringValue, commonName: sp["commonName"].stringValue, id: id, imageName: imageName, family: ""))
            }
        }
        
        return spArr
    }
}

struct Index {
    let name: String
    let id: String
    
    init(id: String, name: String) {
        self.id = id
        self.name = name
    }
}

struct Species {
    let genusSpecies: String
    let commonName: String
    let id: String
    let imageName: String
    let family: String
    
    init(genusSpecies: String, commonName: String, id: String, imageName: String, family: String) {
        self.genusSpecies = genusSpecies
        self.commonName = commonName
        self.id = id
        self.imageName = imageName
        self.family = family
    }
}
