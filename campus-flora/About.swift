//
//  About.swift
//  campus-flora
//
//  Created by ERU on 17/02/2016.
//  Copyright © 2016 ERU. All rights reserved.
//

import Foundation
import MessageUI

class About: UIViewController, UIGestureRecognizerDelegate, MFMailComposeViewControllerDelegate {
    
    
    @IBOutlet weak var AboutView: UITextView!
    @IBOutlet weak var CloseButton: UIButton!
    @IBOutlet weak var MailButton: UIButton!
    
    var show = true
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        AboutView.setContentOffset(CGPointMake(0, 0), animated: false) // make sure about view starts at top
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let data = ["familyTotal": JSONParser.sharedInstance.totalFamilies, "speciesTotal": JSONParser.sharedInstance.totalSpecies, "plantTotal": JSONParser.sharedInstance.totalPlants]
        let toRender = try? GRMustacheTemplate.renderObject(data, fromResource: "about", bundle: NSBundle.mainBundle())
        // Define the font to use
        //let font:UIFont? = UIFont(name: "HelveticaNeue", size: 12.0)
        let attrString = try? NSAttributedString(
            data: toRender!.dataUsingEncoding(NSUnicodeStringEncoding, allowLossyConversion: true)!,
            options: [
                NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType
            ],
            documentAttributes: nil)
        AboutView.attributedText = attrString
        AboutView.tintColor = UIColor.rgb(0, 122, 255)
        let tap = UITapGestureRecognizer(target: self, action: #selector(About.toggleAbout))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        tap.delegate = self
        
        CloseButton.addTarget(self, action: #selector(About.close), forControlEvents: .TouchUpInside)
        MailButton.addTarget(self, action: #selector(About.sendMail), forControlEvents: .TouchUpInside)
    }
    
    // toggle
    func toggleAbout() {
        if (show) {
            let parent = self.parentViewController!.view
            AboutView.setContentOffset(CGPointMake(0, 0), animated: false) // make sure about view starts at top
            parent!.addSubview(view)
            show = false
        }
        else {
            view.removeFromSuperview()
            show = true
        }
    }
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
        if touch.view == MailButton {
            return false
        }
        else if touch.view == AboutView {
            return false
        }
        
        return true
    }

    // close
    func close() {
        show = false
        toggleAbout()
    }
    
    // send mail
    func sendMail() {
        if MFMailComposeViewController.canSendMail() {
            let mailer : MFMailComposeViewController = MFMailComposeViewController()
            mailer.mailComposeDelegate = self
            mailer.setSubject("General Feedback")
            mailer.setToRecipients(["Campus.Flora@sydney.edu.au"])
            
            mailer.setMessageBody("Hi,\n\n\n\nThank you for your feedback. The Campus Flora project team will be using your feedback to help shape future developments.", isHTML: false)
            
            self.modalPresentationStyle = UIModalPresentationStyle.FullScreen
            self.presentViewController(mailer, animated: true, completion: nil)
        }
        else {
            let alert = UIAlertController(title: "Unable to send email", message: "Please check your settings!", preferredStyle: .Alert)
            let action = UIAlertAction(title: "OK", style: .Default, handler: {_ in })
            alert.addAction(action)
            self.presentViewController(alert, animated: true, completion: nil)
        }

    }

    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}