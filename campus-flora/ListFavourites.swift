//
//  ListFavourites.swift
//  campus-flora
//
//  Created by ERU on 16/02/2016.
//  Copyright © 2016 ERU. All rights reserved.
//

import Foundation

//
//  ListFamilies.swift
//  CampusFlora
//
//  Created by ERU on 17/11/2015.
//  Copyright © 2015 Campus Flora. All rights reserved.
//

import UIKit

class ListFavourites: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var favourites: [Species]!
    
    @IBOutlet weak var Table: UITableView!
    
    override func viewWillAppear(animated: Bool) {
        favourites = JSONParser.sharedInstance.speciesFromFavourites()
        Table.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Table.registerClass(UITableViewCell.self, forCellReuseIdentifier: familyIdentifier)
        Table.delegate = self
        Table.dataSource = self
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return favourites.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: favouriteIdentifier)

        cell.textLabel!.text = favourites[indexPath.row].genusSpecies
        cell.detailTextLabel!.text = favourites[indexPath.row].commonName
        cell.imageView!.image = UIImage(named: favourites[indexPath.row].imageName)
        if (favourites[indexPath.row].imageName != "noImageAvailable-thumb") {
            JMImageCache.sharedCache().imageForURL(NSURL(string: favourites[indexPath.row].imageName)!, completionBlock: { (image) in
                cell.imageView!.image = image
            })
            cell.imageView!.contentMode = UIViewContentMode.ScaleAspectFit
        }
        
        cell.accessoryView = UIImageView(image: UIImage(named: "species_detail"))
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let info = JSONParser.sharedInstance.findSpeciesByID(favourites[indexPath.row].id)
        let nav = navigationController as! CFNavigation
        nav.nextScreen(.detail, current: .favourites, sp: info)
    }
    
    // home actions
    func goHome() {
        let nav = self.navigationController as! CFNavigation
        nav.nextScreen(.home)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}