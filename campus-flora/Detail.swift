//
//  Detail.swift
//  CampusFloraIOS
//
//  Created by ERU on 12/11/2015.
//  Copyright © 2015 ERU. All rights reserved.
//

import UIKit
import MessageUI

class Detail: UIViewController, MFMailComposeViewControllerDelegate {
    
    @IBOutlet weak var ImageView: UIImageView!
    @IBOutlet weak var InfoView: UITextView!
    @IBOutlet weak var LeftButton: UIButton!
    @IBOutlet weak var RightButton: UIButton!
    
    var favButton: UIBarButtonItem!
    
    var species = JSON(data: NSData())
    var images: [UIImage]!
    var imageIndex: Int = 0
    
    var previousScreen: screenItem!
    var favourited: Bool!
    
    var detailOnce: Bool = false // slight hack to only reset infoview to top once
    
    // make sure infoview starts at the top
    override func viewDidLayoutSubviews() {
        if (detailOnce) {
            InfoView.setContentOffset(CGPointMake(0,0), animated: false)
            detailOnce = false
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        let pictures = species["images"]
        if pictures.count != 0 {
            showImages(pictures)
        }
        
        favourited = JSONParser.sharedInstance.speciesFavourited(species["id"].stringValue)
        
        if (favourited!) {
            favourited = false
            favButton.tintColor = UIColor.rgb(71, 100, 129)
        }
        else {
            favourited = true
            favButton.tintColor = UIColor.lightGrayColor()
        }
        
        detailOnce = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.title = species["genusSpecies"].stringValue
        //print("phylogeny: \(species["family"]["phylogeny"].stringValue)")
        let toRender = try? GRMustacheTemplate.renderObject(species.dictionaryObject, fromResource: "detail", bundle: NSBundle.mainBundle())
        // Define the font to use
        //let font:UIFont? = UIFont(name: "HelveticaNeue", size: 12.0)
        let attrString = try? NSAttributedString(
            data: toRender!.dataUsingEncoding(NSUnicodeStringEncoding, allowLossyConversion: true)!,
            options: [
                NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType
            ],
            documentAttributes: nil)
        InfoView.attributedText = attrString
        InfoView.tintColor = UIColor.rgb(0, 122, 255)
        //print(species.dictionaryObject)
        //print(attrString)
        ImageView.contentMode = UIViewContentMode.ScaleAspectFit
    }
    
    // images
    func showImages(pictures: JSON) {
        dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_USER_INITIATED.rawValue), 0)) {
            self.images = []
            self.imageIndex = 0
            
            for (_, image): (String, JSON)  in pictures {
                let string = "http://campusflora.sydneybiology.org/" + image["image_url"].string!
                let url = NSURL(string: string)
                let data = NSData(contentsOfURL: url!)
                self.images.append(UIImage(data: data!)!)
            }
            
            dispatch_async(dispatch_get_main_queue()) {
                self.ImageView.image = self.images[self.imageIndex]
                self.ImageView.userInteractionEnabled = true
                
                let left = UISwipeGestureRecognizer(target: self, action: Selector("rotateImage:"))
                let right = UISwipeGestureRecognizer(target: self, action: Selector("rotateImage:"))
                
                let tap = UITapGestureRecognizer(target: self, action: "expandImage")
                
                left.direction = .Left
                right.direction = .Right
                
                self.ImageView.addGestureRecognizer(left)
                self.ImageView.addGestureRecognizer(right)
                self.ImageView.addGestureRecognizer(tap)
                
                self.LeftButton.addTarget(self, action: "rotateLeft", forControlEvents: .TouchUpInside)
                self.RightButton.addTarget(self, action: "rotateRight", forControlEvents: .TouchUpInside)
            }
        }
    }
    
    // image
    func rotateImage(sender: UISwipeGestureRecognizer) {
        if sender.direction == .Left {
            rotateLeft()
        }
        else {
            rotateRight()
        }
    }
    
    func rotateLeft() {
        var index = imageIndex
        if (index < images.count - 1) {
            index += 1
            ImageView.image = images[index]
        }
        else {
            index = 0
            ImageView.image = images[0]
        }
        
        imageIndex = index
    }
    
    func rotateRight() {
        var index = imageIndex
        if (index > 0) {
            index -= 1
            ImageView.image = images[index]
        }
        else {
            index = images.count - 1
            ImageView.image = images[images.count - 1]
        }
        imageIndex = index
    }
    
    func expandImage() {
        EXPhotoViewer.showImageFrom(ImageView, withIndex: Int32(imageIndex), andImages: images)
        //EXPhotoViewer.ex . .showImageFrom(ImageView)
    }
    
    // button actions
    func showMap() {
        let nav = self.navigationController as! CFNavigation
        print(JSON([species["id"].stringValue: species]))
        nav.nextScreen(.map, current: previousScreen, sp: [species["id"].stringValue]) // adding previous screen is a bit of a hack but it helps in the long run
    }
    
    func toggleFav() {
        JSONParser.sharedInstance.toggleFavourites(species["id"].stringValue)
        
        if (favourited!) {
            favourited = false
            favButton.tintColor = UIColor.rgb(71, 100, 129)
        }
        else {
            favourited = true
            favButton.tintColor = UIColor.lightGrayColor()
        }
    }
    
    func sendMail() {
        let name = species["genusSpecies"].string!
        if MFMailComposeViewController.canSendMail() {
            let mailer : MFMailComposeViewController = MFMailComposeViewController()
            mailer.mailComposeDelegate = self
            mailer.setSubject("Feedback about \(name)")
            mailer.setToRecipients(["Campus.Flora@sydney.edu.au"])
            
            mailer.setMessageBody("Hi,\n\n\n\nThank you for your feedback. The Campus Flora project team will be using your feedback to help shape future developments.", isHTML: false)
            
            self.modalPresentationStyle = UIModalPresentationStyle.FullScreen
            self.presentViewController(mailer, animated: true, completion: nil)
        }
        else {
            let alert = UIAlertController(title: "Unable to send email", message: "Please check your settings!", preferredStyle: .Alert)
            let action = UIAlertAction(title: "OK", style: .Default, handler: {_ in })
            alert.addAction(action)
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    func shareSpecies() {
        let text = "I found \(species["genusSpecies"]) on campus! http://campusflora.sydneybiology.org/species/\(species["slug"].stringValue)"
        //let url = NSURL(string: "campusflora.sydneybiology.org/species/\(species["slug"].stringValue)")
        //print(text)
        let activity = UIActivityViewController(activityItems: [text], applicationActivities: nil)
        
        self.presentViewController(activity, animated: true, completion: nil)
    }
    
    // mail
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // go back
    func goBack() {
        let nav = self.navigationController as! CFNavigation
        nav.nextScreen(previousScreen)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}